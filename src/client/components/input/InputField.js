/*
 * Copyright (C) VHSoft, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Vasyl Hoshovsky <vasyl.hoshovsky@vhsoft.com.ua>
 */

/**
 * @Author Vasyl Hoshovsky <vasyl.hoshovsky at vhsoft.com.ua>
 */
import React, { Component } from 'react';
import ClassNames from 'classnames';

export default class extends Component {
    
    render() {
        const {value, placeholder, onChange, additionalClass} = this.props;
        
        let className = ClassNames("form-control", additionalClass);
        
        return (
            <input value={value}
                   placeholder={placeholder}
                   onChange={onChange}
                   className={className}
            />
        )
    }
}