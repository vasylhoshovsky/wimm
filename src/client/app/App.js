/*
 * Copyright (C) VHSoft, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Vasyl Hoshovsky <vasyl.hoshovsky@vhsoft.com.ua>
 *
 */

/**
 * @Author Vasyl Hoshovsky <vasyl.hoshovsky at vhsoft.com.ua>
 */


import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Header from './common/Header';
import * as globalActions from './common/GolobalActions';
var classNames = require('classnames');


const mapStateToProps = (state)=> {
    return {
        fetchesInProgress: state.GlobalReducer.fetchesInProgress
    }
};

const mapDispatchToProps = (dispatch)=> {
    return {
        globalActions: bindActionCreators(globalActions, dispatch)
    }
};

@connect(mapStateToProps, mapDispatchToProps)
export default class extends Component {

    render() {
        let ajaxLoaderClass = classNames('wait', {'hidden': !this.props.fetchesInProgress.length});

        return (
            <div>
                <Header />
                <div className="content-wrapper">
                    <div className="row">
                        <div className="span12">
                            <div className="content">
                                {this.props.children}
                            </div>
                        </div>
                    </div>
                </div>

                <div className={ajaxLoaderClass}>
                    <div>
                        <h6>Please wait...</h6>
                        <img src={require("../img/ajax-loader.gif")} alt="loading..."/>
                    </div>
                </div>
            </div>
        )
    }
}
