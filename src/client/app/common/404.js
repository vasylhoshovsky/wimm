/*
 * Copyright (C) VHSoft, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Vasyl Hoshovsky <vasyl.hoshovsky@vhsoft.com.ua>
 *
 */

/**
 * @Author Vasyl Hoshovsky <vasyl.hoshovsky at vhsoft.com.ua>
 */

import React, {Component} from "react";
import {connect} from "react-redux";


export default class NotFound extends Component {
    render() {
        return (
            <div>
                <div className="container-fluid body-align-center">
                    <div className="row-fluid">
                        <div className="col-lg-12">
                            <div className="centering text-center error-container">
                                <div className="text-center">
                                    <h2 className="without-margin">
                                        Don't worry. It's <span className="text-warning"><big>404</big></span> error only.
                                    </h2>
                                    <h4 className="text-warning">Not found</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}