/*
 * Copyright (C) VHSoft, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Vasyl Hoshovsky <vasyl.hoshovsky@vhsoft.com.ua>
 *
 */

/**
 * @Author Vasyl Hoshovsky <vasyl.hoshovsky at vhsoft.com.ua>
 */

export const FETCH_STARTED = 'FETCH_STARTED';
export const FETCH_FINISHED = 'FETCH_FINISHED';

export function fetchStarted(url) {
    return {
        type: FETCH_STARTED,
        payload: url
    }
}

export function fetchFinished(url) {
    return {
        type: FETCH_FINISHED,
        payload: url
    }
}