/*
 * Copyright (C) VHSoft, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Vasyl Hoshovsky <vasyl.hoshovsky@vhsoft.com.ua>
 *
 */

/**
 * @Author Vasyl Hoshovsky <vasyl.hoshovsky at vhsoft.com.ua>
 */

import React, { Component } from 'react';


export default class extends Component {

    render() {
        return (
            <div className="page-header">
                <div className="header-title">Where is my money?</div>

                <div className="menu-panel">

                    <div className="menu-element">Link</div>
                    <div className="menu-element active">Link</div>
                    <div className="menu-element">Link</div>
                    <div className="menu-element">Link</div>
                    <div className="menu-element">Link</div>

                </div>
            </div>
        );
    }
}