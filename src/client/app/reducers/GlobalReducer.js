/*
 * Copyright (C) VHSoft, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Vasyl Hoshovsky <vasyl.hoshovsky@vhsoft.com.ua>
 *
 */

/**
 * @Author Vasyl Hoshovsky <vasyl.hoshovsky at vhsoft.com.ua>
 */
import {FETCH_STARTED, FETCH_FINISHED} from "../common/GolobalActions";
import _ from "lodash";

const initialState = {
    fetchesInProgress: []
};

export default function GlobalReducer(state = initialState, action) {
    switch (action.type) {
        case FETCH_STARTED:
            let fetches = state.fetchesInProgress.concat([action.payload]);
            return {...state, fetchesInProgress: fetches};
        case FETCH_FINISHED:
            let filtered = _.filter(state.fetchesInProgress, function(e) {
                return e !== action.payload;
            });
            return {...state, fetchesInProgress: filtered};

    }
    return state
}