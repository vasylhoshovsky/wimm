/*
 * Copyright (C) VHSoft, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Vasyl Hoshovsky <vasyl.hoshovsky@vhsoft.com.ua>
 *
 */

/**
 * @Author Vasyl Hoshovsky <vasyl.hoshovsky at vhsoft.com.ua>
 */

import {combineReducers} from "redux";
import {routerReducer} from "react-router-redux";
import GlobalReducer from "./GlobalReducer";

export default combineReducers({
    routing: routerReducer,
    GlobalReducer
});