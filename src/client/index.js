/*
 * Copyright (C) VHSoft, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Vasyl Hoshovsky <vasyl.hoshovsky@vhsoft.com.ua>
 *
 */

/**
 * @Author Vasyl Hoshovsky <vasyl.hoshovsky at vhsoft.com.ua>
 */

import React from "react";
import { render } from "react-dom";
import { Router, Route, IndexRoute, Link, browserHistory } from "react-router";
import { Provider } from "react-redux";
import { syncHistoryWithStore } from "react-router-redux";
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import rootReducer from "./app/reducers/CombineReducer";
import App from "./app/App";
import HomeContainer from "./app/home/HomeContainer";
import NotFound from "./app/common/404";

import "babel-polyfill";
require("bootstrap-loader");
require("./style/main.less");


const INITIAL_STATE = {

};

const store = createStore(rootReducer, INITIAL_STATE, applyMiddleware(thunk));
const history = syncHistoryWithStore(browserHistory, store);

render(
    <Provider store={store}>
        <Router history={history}>
            <Route path="/" component={App}>
                <IndexRoute component={HomeContainer}/>

                <Route path="home" component={HomeContainer}/>
                
                <Route path="*" component={NotFound}/>
            </Route>
        </Router>
    </Provider>,
    document.getElementById('root')
);